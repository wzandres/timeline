<?php

namespace ProgrammerCity\Timeline;

use Illuminate\Support\ServiceProvider;

class TimelineServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->publishes([__DIR__.'/migrations' => base_path('database/migrations')]);
        $this->publishes([__DIR__.'/views' => base_path('resources/views')]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('ProgrammerCity\Timeline\TimelineController');
    }
}
