<?php

namespace ProgrammerCity\Timeline;

use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    public function User(){
        return $this->belongsTo('App\User');
    }
}
