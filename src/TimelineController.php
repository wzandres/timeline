<?php

namespace ProgrammerCity\Timeline;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;


class TimelineController extends Controller
{

    private $adminEmailAlertsTo;

    private $types = ['INFO', 'SUCCESS', 'WARNING', 'DANGER', 'ERROR', 'FATAL', 'SECURITY', 'SYSTEM', 500, 504, 401, 404, 405];

    public function add($logType, $message, User $user){

        $logType = !is_int($logType) ? strtoupper($logType) : $logType;

        if(!in_array($logType, $this->types, true)){
            $this->create('FATAL', "Received response $logType with message: $message", $user);
        } else {

            switch ($logType){
                case "FATAL":
                case "SECURITY":
                case "DANGER":
                case 500:
                    $this->alertByEmail($message, $logType, $user);
                    break;
            }

            $this->create($logType, htmlspecialchars($message), $user);
        }


    }

    private function create($logType, $message, User $user){

        $request = \Illuminate\Http\Request::capture();

        $timeline = new Timeline();
        $timeline->type = $logType;
        $timeline->message = $message;
        $timeline->User()->associate($user);

        if( !is_null($request->fullUrl()) && !empty($request->fullUrl()) ) {
            $timeline->url = $request->fullUrl();
        }

        if( !is_null($request->ip()) && !empty($request->ip()) ) {
            $timeline->client_ip  = $request->ip();
        }

        if( !is_null($request->header('User-Agent')) && !empty($request->header('User-Agent')) ) {
            $timeline->client_browser = $request->header('User-Agent');
        }

        return $timeline->save();

    }

    public function get($userId){

        $user = User::find($userId);
        if($user){
            $timeline = Timeline::where("user_id","=",$user->id)->orderBy("created_at","DESC")->get()->groupBy(function($date) {
                return Carbon::parse($date->created_at)->format('d M Y');
            });
            return Response::json(['timeline' => $timeline]);

        } else { return Response::json(['error' => 'The user was not found.'], 404); }

    }

    public function getByType($type, $userId = null){

        if(!is_null($userId)){
            $timeline = Timeline::where(["user_id" => $userId, "type" => $type])->orderBy("created_at","DESC")->get()->groupBy(function($date) {
                return Carbon::parse($date->created_at)->format('d M Y');
            });
        } else {
            $timeline = Timeline::where(["type" => $type])->orderBy("created_at","DESC")->get()->groupBy(function($date) {
                return Carbon::parse($date->created_at)->format('d M Y');
            });
        }

        if($timeline && $timeline){
            return Response::json(['timeline' => $timeline]);
        } else { return Response::json(['error' => 'There are no results.'], 404); }

    }


    private function alertByEmail($errorMessage, $errorType, User $user){

        $request = \Illuminate\Http\Request::capture();

        $this->adminEmailAlertsTo = env('AdminErrorAlertsEmail');
        Log::info("Attempting to send alert to: " . $this->adminEmailAlertsTo);

        $defaultView = 'emails.systemAlert';
        if(!View::exists($defaultView)){
            $defaultView = 'ProgrammerCity.timeline.systemAlert';
        }

        Mail::send($defaultView, array('errorMessage' => $errorMessage, 'request' => $request, 'errorType' => $errorType, 'user' => $user), function($message) use ($request, $errorType){
         $message->to($this->adminEmailAlertsTo, "Administrator Alerts")
             ->subject("Error type '$errorType' has been produced.");
        });

        if(Mail::failures()){
            foreach(Mail::failures() as $email_address) {
                Log::info(" - $email_address");
            }
        }
    }

}
