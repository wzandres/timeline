<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 8/31/17
 * Time: 9:56 PM
 */

Route::get('/timeline/{userId}', 'ProgrammerCity\Timeline\TimelineController@get');
Route::get('/timeline/type/{type}', 'ProgrammerCity\Timeline\TimelineController@getByType');
Route::get('/timeline/type/{type}/{userId}', 'ProgrammerCity\Timeline\TimelineController@getByType');